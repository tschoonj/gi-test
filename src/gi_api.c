#include "gi_api.h"
#include <glib.h>


/**
 * gi_test_foo_new: (constructor)
 *
 * Returns: (transfer full): newly allocated struct
 */
gi_test_foo* gi_test_foo_new(void) {
	return g_malloc0(sizeof(gi_test_foo));
}

void gi_test_foo_copy(gi_test_foo *A, gi_test_foo **B) {
	if (A == NULL)
		return;
	*B = g_memdup(A, sizeof(gi_test_foo));
}

void gi_test_foo_free(gi_test_foo *foo) {
	g_free(foo);
}
