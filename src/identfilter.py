import sys

def to_camel_case(text):
    if not text.startswith('gi_test_'):
        return text
    res = []
    for token in text.split('_'):
        uc_token = token.title()

        res.append(uc_token)

    return ''.join(res)

if __name__ == '__main__':
    in_text = sys.stdin.read()
    out_text = to_camel_case(in_text)
    sys.stdout.write(out_text)
    with open("ident.txt", "a") as f:
        f.write("in: {} out: {}\n".format(in_text, out_text))
