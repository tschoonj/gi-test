#include "gi_gobject.h"
#include "gi_api.h"


#define GI_TEST_DEFINE_BOXED_TYPE(TypeName, type_name) \
  static gpointer gi_test_ ## type_name ## _copy_internal(gpointer boxed) { \
        gi_test_ ## type_name *A = boxed; \
        gi_test_ ## type_name *B = NULL; \
        gi_test_ ## type_name ## _copy(A, &B); \
        return B; \
  } \
  \
  static void gi_test_ ## type_name ## _free_internal(gpointer boxed) { \
        gi_test_ ## type_name ## _free((gi_test_ ## type_name *) boxed); \
  } \
  GType \
    gi_test_ ## type_name ## _get_type (void) \
  { \
    static volatile gsize gi_test_define_id__volatile = 0; \
    if (g_once_init_enter (&gi_test_define_id__volatile)) \
      { \
        GType gi_test_define_id = \
          g_boxed_type_register_static (g_intern_static_string (#TypeName), \
                                        (GBoxedCopyFunc) gi_test_ ## type_name ## _copy_internal, \
                                        (GBoxedFreeFunc) gi_test_ ## type_name ## _free_internal); \
        g_once_init_leave (&gi_test_define_id__volatile, gi_test_define_id); \
      } \
    return gi_test_define_id__volatile; \
  }

GI_TEST_DEFINE_BOXED_TYPE(GiTestFoo, foo);
