#ifndef GI_API_H
#define GI_API_H

typedef struct _gi_test_foo gi_test_foo;

/**
 * gi_test_foo:
 * @just_an_int: just an int
 *
 * our main struct
 */
struct _gi_test_foo {
	int just_an_int;
};

gi_test_foo* gi_test_foo_new(void);

void gi_test_foo_copy(gi_test_foo *A, gi_test_foo **B);

void gi_test_foo_free(gi_test_foo *foo);

#endif
