#ifndef GI_GOBJECT_H
#define GI_GOBJECT_H

#include <glib-object.h>

#define GI_TEST_TYPE_FOO (gi_test_foo_get_type ())
GType gi_test_foo_get_type(void) G_GNUC_CONST;

#endif
