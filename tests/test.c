#include "gi_api.h"
#include <assert.h>
#include <stddef.h>

int main(int argc, char *argv[]) {

	gi_test_foo *foo = gi_test_foo_new();
	foo->just_an_int = 5;
	gi_test_foo *foo_copy = NULL;
	gi_test_foo_copy(foo, &foo_copy);
	assert(foo->just_an_int == foo_copy->just_an_int);
	gi_test_foo_free(foo);
	gi_test_foo_free(foo_copy);

	return 0;
}
